# Authenticate

Get a token at https://phabricator.collabora.com/conduit/login/

Add the following in your  ~/.arcrc

    {
      "hosts": {
        "https://phabricator.collabora.com/api/": {
          "token": "<YOUR_TOKEN_HERE>"
        }
      }
    }

Or, if you have arcanist installed, you can authenticate with:

    arc install-certificate https://phabricator.collabora.com

# Python install dependencies

    pip install phabricator python-dateutil

# Run script

    ./phab-epic-comments.py --project GOO0012 --week 20

or

    ./phab-epic-comments.py --project GOO0012 --week 20 -a

or

    ./phab-epic-comments.py --project GOO0012 --since "10 Aug 2018" --until "2018/08/18 10AM"

# Convert to other formats

Install pandoc and run:

    ./phab-epic-comments.py -p GOO0012 -w 20 -o report.odt

or

    ./phab-epic-comments.py -p GOO0012 -w 20 -o report.html

See pandoc's documentation for all the supported output formats.
