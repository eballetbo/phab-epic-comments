#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import subprocess
import sys
import time
import tempfile
import dateutil.parser
from datetime import datetime, timedelta
from phabricator import Phabricator

phid_author = {
    "PHID-USER-bbu4ndhblo75po4uxm6k": "Guillaume Tucker",
    "PHID-USER-vggh54ukp3j7gparyngh": "Ana Guerrero López",
    "PHID-USER-x74hj2myyv5orgnkqzjq": "Ezequiel Garcia",
    "PHID-USER-fqa2366w5ytfnrwhmgvt": "Enric Balletbò i Serra",
    "PHID-USER-jj2vhn3mtl3fcequp4kk": "Helen Koike Fornazier",
    "PHID-USER-2vftogflsj7nyma6fnxs": "Gaël PORTAY",
    "PHID-USER-lwgnagoa6ylvmdw2z3ub": "Fabien Lahoudere",
    "PHID-USER-hprdoftt2fep6m4it4ep": "Michał Gałka",
    "PHID-USER-ku24h4d66plfdnaeeu5s": "Boris Brezillon",
    "PHID-USER-wkqc2znmkowg6kzkceae": "Tomeu Vizoso",
    "PHID-USER-wzioca2xspa6f55twtyj": "Gustavo Padovan",
    "PHID-USER-q44elrwcztgqo2yzqaoo": "Nicolas Dufresne",
    "PHID-USER-gyi5hqju2r3x7gek7shu": "Andrzej Pietrasiewicz",
    "PHID-USER-i3gtnmf24upymctfypox": "Alyssa Rosenzweig",
}


def extract_comments(transactions, since_epoch, until_epoch):
    c = {}
    for t in transactions:
        epoch = int(t['dateCreated'])
        if t['comments'] and epoch >= since_epoch and epoch <= until_epoch:
            # Use epoch as dictionary index so we can order items
            # chronologically if needed
            c[epoch] = {
                'author': phid_author[t['authorPHID']],
                'comment': t['comments']
            }
    return c


def pprint_comments(epics, task, comments, nophab, f):
    if not comments:
        return

    f.write("\n### {}\n".format(epics[int(task)]))
    for k, v in comments.items():
        if v['author']:
            f.write("\n*By {}:*\n".format(v['author']))
        if v['comment']:
            f.write("\n{}\n".format(v['comment']))
    f.write("\n")

    if nophab:
        return

    link = "https://phabricator.collabora.com/T{}".format(task)
    f.write("[{}]({})\n".format(link, link))


def main(args):
    phab = Phabricator()  # This will use your ~/.arcrc file
    phab.user.whoami()
    # year, first day of the week, number of the week, hour, timezone
    s_date = datetime.strptime("{} 2 {} 8".format(args.year, args.week),
                               "%Y %w %W %H")
    e_date = datetime.strptime("{} 2 {} 8".format(args.year, args.week + 1),
                               "%Y %w %W %H")
    e_date = e_date - timedelta(seconds=1)

    if args.since:
        s_date = dateutil.parser.parse(args.since)
    if args.until:
        e_date = dateutil.parser.parse(args.until)

    s_epoch = int(s_date.strftime('%s'))
    e_epoch = int(e_date.strftime('%s'))

    if args.output:
        fd, tmp_file_name = tempfile.mkstemp(suffix=".md")
        f = open(tmp_file_name, "w")
    else:
        f = sys.stdout

    if args.title:
        f.write("## {}\n".format(args.title))
    else:
        f.write("Report: from {} to {}\n".format(s_date, e_date))

    query = {
        "constraints": {
            "name": args.project
        }
    }

    project_phid = phab.project.search(**query).response["data"][0]["phid"]

    query = {
        "queryKey": "open",
        "constraints": {
            "projects": [project_phid]
        }
    }
    tasks = phab.maniphest.search(**query).data
    epics = {}

    for task in tasks:
        if args.filter in task["fields"]["name"]:
            epics[task["id"]] = task["fields"]["name"]

    d_transactions = phab.maniphest.gettasktransactions(
        ids=list(epics.keys())).response

    for task in d_transactions:
        comments = extract_comments(d_transactions[task], s_epoch, e_epoch)
        pprint_comments(epics, task, comments, args.nophab, f)

    if f is not sys.stdout:
        f.close()
        os.close(fd)

    if args.output:
        try:
            subprocess.check_call(["pandoc", tmp_file_name, "-o", args.output])
            os.remove(tmp_file_name)
        except OSError:
            sys.stderr.write("WARNING: pandoc is not installed, \
                              could not convert the file format.")
            os.rename(tmp_file_name, args.output)

    return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="""
Retrieve all comments in a given week from EPIC tasks""")
    parser.add_argument('-p', '--project', type=str, required=True,
                        help="the project to retrieve the comments")
    parser.add_argument('-y', '--year', type=int, default=datetime.now().year,
                        help="the year to retrieve the comments \
                             (default:current_year)")
    parser.add_argument('-a', '--all', action='store_true',
                        help="also list tasks that weren't updated")
    parser.add_argument('-o', '--output', metavar="FILE",
                        help="output to a file instead of stdout")
    parser.add_argument('-f', '--filter', metavar="STRING", default="EPIC",
                        help="filter tasks containing STRING in the title \
                        (default:%(default)s)")
    parser.add_argument('-w', '--week', type=int,
                        default=datetime.now().isocalendar()[1],
                        help="the number of the week to retrieve the comments \
                        (default:current_week)")
    parser.add_argument('-s', '--since',
                        help="retrieve comments since a date")
    parser.add_argument('-u', '--until',
                        help="retrieve comments until a date")
    parser.add_argument('-t', '--title', metavar="STRING",
                        help="set the title of the report")
    parser.add_argument('--nophab', action='store_true',
                        help="don't show phabricaton information")
    args = parser.parse_args()

    res = main(args)
    sys.exit(0 if res is True else 1)
